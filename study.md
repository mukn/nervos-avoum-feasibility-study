# AVOUM for Nervos Feasibility Study

This document describes the results of a feasibility study, per Step 1
of the [AVOUM for Nervos](https://j.mp/AVOUMforNervos) project.

* [Background](#background)
* [Summary](#Summary)
* [Protocol and Interface Modifications](#Protocol-and-Interface-Modifications)
  * [JSON-RPC Modifications](#JSON-RPC-Modifications)
  * [Rebase Script Execution Environment](#Rebase-Script-Execution-Environment)
* [Proof of Concept](#Proof-of-Concept)
  * [Limitations](#Limitations)
  * [Transaction Format](#Transaction-Format)
  * [Notation](#Notation)
  * [Common Definitions](#Common-Definitions)
  * [Type Script](#Type-Script)
  * [Rebase Script](#Rebase-Script)
  * [Lock Scripts](#Lock-Scripts)
    * [Common Definitions](#Lock-Scripts-Common-Definitions)
    * [Escrowed Lock Script](#Escrowed-Lock-Script)
    * [Submit Lock Script](#Submit-Lock-Script)
    * [Release Lock Script](#Release-Lock-Script)

<a name="background"></a>
## Background

Nervos is a UTXO Blockchain, like Bitcoin but unlike the account/balance model
used by Ethereum. This has advantages, such as parallel verification of
transactions, but it means that contracts have no built-in notion of a stable
identity, and transactions are only valid if they operate on the current state
of a contract. In contrast, with the account/balance model, transactions
cannot be verified in parallel, but a transaction can be applied to a contract
even if there have been intervening transactions since it was constructed.

This causes problems if there is contention on a contract, as the author of a
transaction must continually monitor the blockchain and retry submitting the
transaction after each intervening change. This can result in denial of service
vulnerabilities if the participants lack a fair amount of technical
sophistication, and this has shown itself to be a problem in practice.

The Account-View-On-UTXO-Model (AVOUM) approach solves this by providing a way
to support "malleable" transactions, which can be submitted to a miner, who is
then able to update the transaction in order to solve conflicts with intervening
transactions, rather than having to reject the original transaction outright,
thus easing the burden on participants to deal with contention, while still
allowing the benefits of the UTXO model.

For more background information see the [AVOUM for
Nervos](https://j.mp/AVOUMforNervos) document.

<a name="summary"></a>
## Summary

To summarize the study's findings:

- No modifications are needed to the transaction format or to the
  validation process to suppport malleable transactions.
- To support malleable transactions, we will need to supply the miner
  with an additional script, beyond the lock & type scripts, which we
  call the "rebase" script, by analogy to the rebase operation in
  version control systems such as Git.
  - The rebase script operates similarly to the lock & type scripts, but
    has access to an expanded system call interface, which enables the
    script to:
     - Write out a new transaction to be applied to the current tip of
       the blockchain, in lieu of the stale original transaction.
     - In addition to being able to read data from the original
       transaction with which the rebase script was submitted, the
       script may also read any applied transactions which consumed the
       inputs expected by the original transaction.
  - The rebase script is submitted to the miners via a new, optional
    argument to the send_transaction rpc call. The presence of this
    argument indicates that a transaction is malleable, and that the
    provided script should be used to rebase it should other
    transactions consume its inputs before it is applied to the
    blockchain.
  - Importantly, the rebase script itself does *not* appear in a
    transaction; it only needs to be recognized and used by rebasing
    miners. Validation depends only on the lock & type scripts, as
    is currently the case.
- In general, malleable transactions will require specialized lock
  scripts, as normal lock scripts disallow malleability by design.
  For regular users with regular lock scripts to use malleable
  transactions thus requires an extra step: first, cells compatible with
  malleability are created in a regular non-malleable transaction.
  Only then can these cells be assembled into malleable transactions.
- Imposing a fixed deadline on a contract as in the simple first price
  auction proof of concept example is not currently possible. We believe
  we have a design for a variation on this proof of concept that is
  implementable and also acceptable.

<a name="Protocol-and-Interface-Modifications"></a>
## Protocol and Interface Modifications

This section describes necessary modifications to Nervos protocols and
interfaces.

<a name="JSON-RPC-Modifications"></a>
### JSON-RPC Modifications

The `send_transaction` json-rpc method will optionally take an additional
argument, `rebase_script`, of type `Script`, specifying the rebase
script if the transaction is intended to be malleable.

Similar changes will be needed anywhere else in the system where a transaction
is submitted and supporting malleable transactions is desired.

<a name="Rebase-Script-Execution-Environment"></a>
### Rebase Script Execution Environment

The rebase script will run in an environment similar to the type & lock
scripts, but with an expanded system call interface.

The rebase script is run when a malleable transaction specifies input
cells which no longer exist, having been consumed by another transaction
that was committed first. We call the transaction with stale inputs
the "original transaction," and the committed transaction which consumed
those inputs the "intervening transaction." This is done iteratively,
until the script outputs a transaction whose inputs are all still live.
For efficiency, we also provide the rebase script with a mechanism to
"skip ahead" to the latest state, if it doesn't need the intermediate
states; see below.

Existing system calls which would be valid in a type or lock script are
still valid in a rebase script, with the same semantics, where
operations which read data from "the transaction" read data from the
original transaction.

For each existing system call that takes a `source` argument and reads data from
a transaction, new values for `source` are recognized. Specifically, for each
existing value for `source`, there is a new value that is also accepted, but
instead of loading data from the original transaction, instead loads it from the
corresponding part of the intervening transaction.

For each system call which loads data from the transaction, but does
*not* take a source argument, we introduce a new system call with the
same semantics, but which reads from the intervening transaction instead
of the original transaction.

We also add a two new system calls. The first appends data to the newly
constructed transaction:

```c
int ckb_emit_tx_data(const void *addr, uint64_t len) {
    return syscall(/* ... */);
}
```

The second changes the focused intervening transaction; it takes the hash
of a `Script` structure, and updates the intervening transaction to refer
to the most recent transaction which includes a cell with that type script:

```c
int ckb_seek_last_tx_by_type(const void *addr, uint64_t len) {
    return syscall(/* ... */);
}
```

The latter is provided for efficiency; there may be many intervening
transactions, and the rebase script may not necessarily need the intermediate
states.

To use this effectively, the type script must ensure that this uniquely
identfies the particular contract. One way to do this is to require a value for
the script's `args` field that incorporates the hash of one of the inputs
to the transaction creating the contract.

`ckb_seek_last_tx_by_type` is tentative; a detailed exploration of the
design space for search primitives might yield more efficient and/or easier to
use options. Nevertheless, we specify a concrete interface for now for the sake
of illustration (it is used in the proof of concept, below). It should be
considered subject to change.

A suitable cost will have to be assigned to each of these system calls.

<a name="Proof-of-Concept"></a>
## Proof of Concept

To illustrate how malleable transactions will work, this section
sketches a proof of concept implementation of a simple English first
price auction with open bids.

The example prioritizes simplicity and clarity over flexibility; some patterns
(such as hard-coded indicies) are likely inappropriate for a real-wolrd
implementation, but are sufficient to illustrate the design.

Our original plan was for this contract to also impose a fixed deadline,
however, due to limitations described in [Limitations](#Limitations), this is
not feasible without further changes. Instead the auction may be shut down after
a fixed deadline, but will continue until a participant manually triggers the
shutdown. We believe this is an acceptable compromise.

We include pseudocode for both a type and rebase script for the auction,
as well as lock scripts suitable for bids & assets.  When it comes time
to first implement this proof of concept, as an incremental step we may
hard-code the rebase logic for this particular contract in the miners,
and introduce proper support for rebase scripts as a subsequent task.

For simplicity, in this example we assume both the assets being sold and
the bids are SUDTs (presumably different kinds), though the techniques
can easily be applied to other kinds of cells.

<a name="Limitations"></a>
### Limitations

Imposing a fixed deadline on the contract requires that the type script
can query some notion of the current time; on Ethereum it would be natural
to use the current block number for this. However, on Nervos there is no way
to query such information. We are under the understanding that this is by
design; providing such a mechanism would preclude validators from validating
transactions before choosing a block to put them in.

Instead of extending the Nervos system to support querying some notion of
blockchain time, we propose a relaxed variant of the contract, where either the
current bidder or the seller may trigger closing the auction any time after the
deadline, but if they do not the auction will continue until someone notifys the
contract that the deadline has passed.

A participant can prove that a deadline is passed by including a reference
in header_deps to a block that is after the deadline; see
[Transaction Format](#Transaction-Format).

<a name="Transaction-Format"></a>
### Transaction Format

A transaction creating the auction takes the form:

- A witness with the value 0, to indicate that we are creating the
  transaction.
- Input cells:
  - A cell holding the assets, owned by the seller.
- Output cells:
  - The cell for the auction consensus, which tracks the state of the
    auction.
  - A cell holding the assets, whose lock script only allows unlocking
    in transactions involving the auction's type script.

A bidder can place a new bid by submitting a transaction formatted as
follows:

- A witness with the value 1, to indicate this is a new bid.
- Input cells, in index order:
  - The cell for the auction consensus, whose type script enforces the
    rules of the auction.
  - The cell holding the assets, with a lock script only allowing the
    auction to consume it.
  - A cell holding the new bid.
  - A cell holding the escrowed old bid. Omitted if no bids have been
    placed yet.
- Output cells, in index order:
  - The new auction consensus, holding the new state of the auction.
  - The cell holding the assets, again with a lock script allowing
    access only by the auction.
  - A cell holding the new bid, locked for use by the auction only.
  - A new cell owned by the previous bidder, as recorded in the auction
    state, holding the previous bid. Omitted if this was the first bid.

After the timeout, the auction can be closed (by either the current
bidder or the seller) via a transaction of the form:

- A witness with the value 2, to indicate this is a closing transaction.
- Input cells:
  - The cell for the auction consensus.
  - The cell holding the assets.
  - The cell holding the current bid.
- Output cells, in index order:
  - A new cell owned by the seller, holding the current bid.
  - A new cell holding the assets, owned by the bidder.
- In `header_deps`, a block whose block number is after the deadline. This
  is to prove that the deadline has indeed passed.

In the above we speak of cells being "owned by" some participant.
Ownership in this case means that the lock script for that cell allows
the owner to consume it, most likely by checking a signature. For input
cells, the lock script must *also* allow the auction to consume the
cells, otherwise rebasing the transaction is not possible. It can do
this just by checking that one of the cells in the transaction has the
auction script's type script, and letting said type script take over.

For cells held in escrow, there must be a lock script assigned
to those cells which *only* allows consumption by the auction.
Making this work is slightly subtle, since both the auction type script
and the escrow lock script have to be able to identify each other. If
this were a one-way relationship, we could just check the hash of the
other script, but since it is two way, we cannot embed the hash of each
script in the code of the other script. Instead, the auction *state*
stores the hash of the lock script, breaking the dependency cycle.

For cells being released from escrow, the auction must choose a lock
script that grants only the desired participant the ability to unlock
the cell.

<a name="Notation"></a>
### Notation

The following sections include pseudo-code, largely inspired by rust syntax, but
not intended to be valid rust, and deviates where it improves clarity.

We also do not delve into the low level details of the exact system calls used
to read (and in the case of the rebase script, write) data. Instead, code
assumes the existence of a variable `tx` (for the type script) or variables
`orig_tx` and `intervening_tx` (for the rebase script), holding already-parsed
versions of the available transaction data. For the rebase script, we assume a
function `emit_tx()` exists, which accepts the transaction data type and
serializes it, bottoming out to the new `ckb_emit_tx_data()` system call.

As mentioned in [Transaction Format](#Transaction-Format), we assume
some notion of ownership for cells representing assets & bids. In
particular, we assume the existence of:

- Some type `OwnerId` which identifies an owner of such a cell.
- A function `owner_of`, which takes a cell, and returns a
  corresponding `OwnerId`. Checking the owner depends on knowledge of
  the lock script's operation, so we assume everyone is using specific
  well-known lock scripts; this function aborts if given a cell with an
  unknown script.

<a name="Common-Definitions"></a>
### Common Definitions

This section contains common definitions used by both the type and rebase scripts.

```rust
/// The current state stored in the cell for the auction consensus.
/// We assume some suitable binary serialization but leave it
/// unspecified.
struct AuctionState {
    /// id for the current highest bidder.
    current_bidder: OwnerId,

    /// Value of the current bid.
    current_bid: u64,

    /// The block after which the auction is considered to have ended.
    /// To distribute the assets/bid, the transaction must prove that
    /// this is in the past.
    deadline_block: u64,

    /// Id for the seller; when the auction is over, the current bid
    /// will be sent here.
    seller_id: OwnerId,

    /// Crytpographic hash of the lock script that we expect escrowed
    /// cells to have.
    escrow_lock_script_hash: Hash,

    /// Likewise, but for cells being transfered into escrow.
    submit_lock_script_hash: Hash,

    /// Likewise, but for cells being released from escrow.
    released_lock_script_hash: Hash,

}

/// opcodes, to denote the type of transaction.

const OP_OPEN = 0; // create the auction
const OP_BID = 1; // new bid
const OP_CLOSE = 2; // terminate the auction

/// Returns the value of the cell, according to the SUDT standard.
fn sudt_value(sudt_cell: Cell) -> usize;

/// Return the owner of a cell. Aborts if the cell is not using the
/// expected lock script. Does not make sense for escrowed cells.
fn owner_of(expected_code_hash: Hash, cell: Cell) -> OwnerId {
    assert!(cell.lock.code_hash == expected_code_hash);
    // We store the owner in the args for the lock script. Most likely this
    // will be a public key. If this is something that doesn't fit in in
    // args field, we can hash it first.
    return cell.lock.args;
}
```

<a name="Type-Script"></a>
### Type Script

```rust
match tx.witnesses[0] {
  OP_OPEN => {
    assert!(tx.input_cells.len() == 1);
    assert!(tx.output_cells.len() == 2);

    let assets_cell_in = tx.input_cells[0];
    let auction_cell = tx.output_cells[0];
    let assets_cell_out = tx.output_cells[1];

    assert!(auction_cell.type == current_script);

    assert!(assets_cell_out.type == assets_cell_in.type);
    assert!(sudt_value(assets_cell_out) == sudt_value(assets_cell_in));

    // Make sure the escrowed cell is using the expected lock script.
    assert!(assets_cell_out.lock.code_hash == auction_cell.escrow_lock_script_hash);

    // Make sure the seller agrees with the input.
    assert!(auction_state.seller_id == owner_of(auction_cell.submit_lock_script_hash,
                                                assets_cell_in));

    let auction_state = decode::<AuctionState>(auction_cell);

    // We use the args field of our type script to provide a unique identity
    // for the auction; because it is equal to the hash of an input to the
    // transaction that created the auciton, it necessarily uniquely identifies
    // the auction. This lets the rebase script find the current auction state
    // by searching for the type script.
    assert!(auction_cell.type.args == hash(assets_cell_in));
  },
  OP_BID => {
    assert!(tx.input_cells.len() == 3);

    let auction_input_cell = tx.input_cells[0];
    let assets_input_cell = tx.input_cells[1];
    let new_bid_input = tx.input_cells[2];

    let auction_output_cell = tx.output_cells[0];
    let assets_output_cell = tx.output_cells[1];
    let new_bid_output = tx.output_cells[2];

    let old_auction_state = decode::<AuctionState>(auction_input_cell.occupied_data);
    let new_auction_state = decode::<AuctionState>(auction_output_cell.occupied_data);

    if old_auction_state.current_bid == 0 {
        // First bid.
        assert!(tx.output_cells.len() == 3);
    } else {
        // Make sure we're properly returning the old bid:
        let old_bid_input = tx.output_cells[3];
        let returned_escrow_output = tx.output_cells[3];
        assert!(old_bid_input.type == BID_TYPE);
        assert!(returned_escrow_output.type == BID_TYPE);
        assert!(sudt_value(old_bid_input) == old_auction_state.current_bid);
        assert!(sudt_value(returned_escrow_output) == old_auction_state.current_bid);
        assert!(owner_of(new_auction_state.released_lock_script_hash,
                         returned_escrow_output) == old_auction_state.current_bidder);
    }

    // Make sure the escrowed cells are using the expected lock script:
    assert!(assets_output_cell.lock.code_hash == auction_input_cell.escrow_lock_script_hash);
    assert!(new_bid_output.lock.code_hash == auction_input_cell.escrow_lock_script_hash);

    assert!(assets_input_cell.type == ASSETS_TYPE);
    assert!(new_escrow_cell.type == BID_TYPE);
    assert!(new_bid_cell.type == BID_TYPE);

    // Make sure the bid is valid:
    let new_bid = sudt_value(new_bid_input);
    assert!(new_bid > old_auction_state.current_bid);
    assert!(sudt_value(new_bid_output) == new_bid);
    assert!(new_auction_state.current_bidder == owner_of(auction_input_cell.submit_lock_script_hash,
                                                         new_bid_cell));

    // Prevent otherwise mucking with the auction state:
    assert!(old_auction_state.seller_id == new_auction_state.seller_id);
    assert!(old_auction_state.deadline_block == new_auction_state.deadline_block);
    assert!(old_auction_state.escrow_lock_script_hash == new_auction_state.escrow_lock_script_hash);
    assert!(old_auction_state.submit_lock_script_hash == new_auction_state.submit_lock_script_hash);
    assert!(old_auction_state.release_lock_script_hash == new_auction_state.release_lock_script_hash);
    assert!(auction_input_cell.type == current_script);
    assert!(auction_input_cell.type == auction_output_cell.type);
    assert!(auction_input_cell.lock == auction_output_cell.lock);
    assert!(assets_input_cell.type == assets_output_cell.type);
    assert!(assets_input_cell.lock == assets_output_cell.lock);
  },
  OP_CLOSE => {
    assert!(tx.input_cells.len() == 3);
    assert!(tx.output_cells.len() == 2);

    let auction_cell = tx.input_cells[0];
    let assets_cell_input = tx.input_cells[1];
    let escrowed_bid_cell = tx.input_cells[2];
    let claimed_bid_cell = tx.output_cells[0];
    let assets_cell_output = tx.output_cells[1];

    assert!(auction_cell.type == current_script);
    let auction_state = decode::<AuctionState>(auction_cell.occupied_data);

    // Make sure the deadline has really passed:
    assert!(auction_state.deadline_block < tx.header_deps[0].block_number);

    // Validate the funds being sent to the seller:
    assert!(claimed_bid_cell.type == BID_TYPE);
    assert!(sudt_value(escrowed_bid_cell) == auction_state.current_bid);
    assert!(sudt_value(claimed_bid_cell) == auction_state.current_bid);
    assert!(owner_of(auction_cell.released_lock_script_hash,
                     claimed_bid_cell) == auction_state.seller_id);

    // Validate the assets being sent to the highest bidder:
    let assets_cell = tx.output_cells[1];
    assert!(assets_cell.type == ASSETS_TYPE);
    assert!(sudt_value(assets_cell_input) == sudt_value(assets_cell_output));
    assert!(owner_of(auction_cell.released_lock_script_hash,
                     assets_cell) == auction_state.current_bidder);
  },
_ => panic!("Invalid operation");
}
```

<a name="Rebase-Script"></a>
### Rebase Script

```rust
// Fast forward to the latest transaction that uses the auction script.
// We could rebase one transaction at a time, but we don't actually need
// the intermediate states, so this is more efficient if there are many
// of them.
//
// Note that per discussion earlier, this includes an args field that
// is guaranteed to uniquely identify this auction.
seek_last_tx_by_type(orig_tx.output_cells[0].script.type);

// Make sure the intervening transaction is a new bid. If instead it closed out
// the auction, there's nothing for us to do; the auction is over. It isn't
// possible for this to be OP_OPEN, since we know the auction has already
// started.
assert!(intervening_tx.witnesses[0] == OP_BID);

match orig_tx.witnesses[0] {
  OP_BID => {
    let orig_auction_cell = orig_tx.output_cells[0];
    let inetervening_auction_cell = intervening_tx.output_cells[0];

    let orig_auction_state = decode::<AuctionState>(
      orig_auction_cell.occupied_data
    );
    let intervening_auction_state = decode::<AuctionState>(
      intervening_auction_cell.occupied_data
    );

    let new_bid_cell = orig_tx.input_cells[2];
    let mut new_auction_state = intervening_auction_state.clone();
    new_auction_state.current_bid = orig_auction_state.current_bid;
    new_auction_state.current_bidder = orig_auction_state.current_bidder;

    let mut new_tx = orig_tx.clone();
    // copy state, assets.
    new_tx.input_cells[0] = intervening_tx.output_cells[0];
    new_tx.input_cells[1] = intervening_tx.output_cells[1];
    // new_tx.input_cells[2] is already new_bid_cell
    new_tx.input_cells[3] = intevening_tx.output_cells[2]; // previous escrowed bid.

    new_tx.output_cells[0] = make_auction_cell(state=new_auction_state);

    // this cell is the assets, which are logically the same, but need to
    // be tweaked just because we need a "different" cell since we're
    // consuming the old one. We assume `bump` does the necessary fiddling.
    // Maybe we could avoid this by putting the assets in deps?
    new_tx.output_cells[1] = bump(intevening_tx.output_cells[1].clone());

    // new_tx.output_cells[2] is the new bid, same as in orig_tx.

    // return the previous bidder's escrowed funds.
    new_tx.output_cells[3] = intervening_tx.output_cells[2].clone();
    new_tx.output_cells[3].lock = Script {
        args: intervening_auction_state.current_bidder,
        code_hash: intervening_auction_state.release_lock_script_hash,
    };

    emit_tx(new_tx);
  },
  OP_CLOSE => {
    let mut new_tx = orig_tx.clone();

    // copy over the state, assets and current bid.
    new_tx.input_cells[0] = intervening_tx.output_cells[0];
    new_tx.input_cells[1] = intervening_tx.output_cells[1];
    new_tx.input_cells[2] = intervening_tx.output_cells[2];

    let auction_state = decode::<AuctionState>(new_tx.input_cells[0].occupied_data);

    // distribute bid to the seller:
    new_tx.output_cells[0] = intervening_tx.ouptut_cells[2].clone();
    new_tx.ouptut_cells[0].lock = Script {
        args: auction_state.seller_id,
        code_hash: auction_state.released_lock_script_hash,
    };

    // distribute assets to the bidder.
    new_tx.output_cells[1] = intervening_tx.output_cells[1].clone();
    new_tx.output_cells[1].lock = Script {
        args: auction_state.current_bidder,
        code_hash: auction_state.released_lock_script_hash,
    };

    emit_tx(new_tx);
  },
  OP_OPEN => {
    panic!("Can't rebase the create operation");
  },
  _ => {
    panic!("Invalid operation");
  },
}
```

<a name="Lock-Scripts"></a>
### Lock Scripts

This section details lock scripts used for various SUDT cells in the
transactions.

<a name="Lock-Scripts-Common-Definitions"></a>
### Common Definitions

We assume a common definition for use in the below lock scripts:

```rust
/// Verify that the specified owner has explicitly signed off on this
/// transaction. The most obvious implementation would be to look for a
/// signature in witnesses. Note that scripts that use this
/// *unconditionally* are incompatible with malleable transactions,
/// because any attempt to rebase will break the signature.
///
/// However, *conditional* use of this can be useful; see the submit
/// lock script for an example.
check_authorized_by_owner(tx: Transaction, owner: OwnerId);
```

<a name="Escrowed-Lock-Script"></a>
#### Escrowed Lock Script

```rust
// This is the lock script used for escrowed cells (i.e. the assets
// being sold and the current highest bid). It just checks that
// the auction consensus is one of the cells in the transaction,
// and relies on the auction type script above to do the rest of
// the checking.
//
// In each of the transaction formats, the auction cell is the first
// cell in either the output cells (for creation), input cells (for
// closing) or both (for bidding), so this is very simple:
assert!(tx.input_cells[0].type.code_hash == AUCTION_TYPE_SCRIPT_CODE_HASH
        || tx.output_cells[0].type.code_hash == AUCTION_TYPE_SCRIPT_CODE_HASH);

```

<a name="Submit-Lock-Script"></a>
#### Submit Lock Script

```rust
// This is the lock script for cells being submitted into escrow. We need to
// allow both the auction type script to unlock these, but also the original
// owner, so they can get back their funds if e.g. a bid doesn't go through.

if(tx.input_cells[0].type.code_hash == AUCTION_TYPE_SCRIPT_CODE_HASH
   || tx.output_cells[0].type.code_hash == AUCTION_TYPE_SCRIPT_CODE_HASH) {
    // Part of the auction; let the auction type script deal with the rest.
    return;
} else {
    // Not part of the auction. The owner may be trying to withdraw the funds,
    // e.g. because their bid was not accepted. Check that the owner has
    // authorized this.
    check_authorized_by_owner(tx, current_script.args);
}
```

<a name="Release-Lock-Script"></a>
#### Submit Lock Script

```rust
// This is the lock script for cells that have been released from escrow.
// These are no longer part of the auction, so they can *only* be used
// if their owner has explicitly authorized the transaction.
check_authorized_by_owner(tx, current_script.args);
```
